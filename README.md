### Jeu inspiré de Ball Blast de VOODOO
- Pour jouer il suffit de cliquer sur l'écran pour déplacer le canon et détruire les boules (possibilité d'améliorer les paramètres du canon)
## Difficultés rencontrées :
- Créer un objet à l'avance sans l'instantiate // j'ai du faire une classe à part pour les crée puis lui
passer les données à l'instantiation (Boule).
## Difficultés non résolu :
- Mettre un Text dans un sprite (avec un TextMeshPro et le sprite en children j'avais réussi à les faire se déplacer ensemble mais pour changer la valeur du Text je n'ai pas réussi)
- Ne pas avoir à doubler des données (notemment listes listePuissance/listeCadence dans fichier clickButton)


## Développé sur UNITY 2020.3.20 par Romain LOPEZ
testé sur Samsung S9


