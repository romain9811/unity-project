using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;	

public class clickButton:MonoBehaviour{	

	public void onClick(){	
		Invoke("goScene3",0.2f);
	}

	public void termineNiveau(){
		Invoke("goScene2",0f);
	}

    void goScene3(){							
        SceneManager.LoadScene("scene3-Game");		
    }

	void goScene2(){							
        SceneManager.LoadScene("scene2-Menu");		
    }

	public void lvlupCadence(){
		List<int> listeCadence =  new List<int>{8,13,19,32,42,1000}; //doublon dans le code à enlever
		int currentPo = PlayerPrefs.GetInt("po", 0);
		int currentLvlCadence = PlayerPrefs.GetInt("cadenceLvl", 1); 
		if (currentPo - listeCadence[currentLvlCadence]>= 0){
            currentPo -= listeCadence[currentLvlCadence];
            PlayerPrefs.SetInt("po", currentPo);
            currentLvlCadence++;
            PlayerPrefs.SetInt("cadenceLvl", PlayerPrefs.GetInt("cadenceLvl", 1) + 1);
            GameObject.FindGameObjectWithTag("po").GetComponent<Text>().text = "" + currentPo;
            GameObject.FindGameObjectWithTag("cadenceLvl").GetComponent<Text>().text = "" + currentLvlCadence;
			GameObject.FindGameObjectWithTag("cadencePo").GetComponent<Text>().text = "" + listeCadence[currentLvlCadence];
        }
	}

	public void lvlupPuissance(){
		List<int> listePuissance = new List<int>{7,22,37,45,73,1000};
		int currentPo = PlayerPrefs.GetInt("po", 0);
		int currentLvlPuissance = PlayerPrefs.GetInt("puissanceLvl", 1);
        if (currentPo - listePuissance[currentLvlPuissance]>= 0){
            currentPo -= listePuissance[currentLvlPuissance];
            PlayerPrefs.SetInt("po", currentPo);
            currentLvlPuissance++;
            PlayerPrefs.SetInt("puissanceLvl", PlayerPrefs.GetInt("puissanceLvl", 1) + 1);
            GameObject.FindGameObjectWithTag("puissanceLvl").GetComponent<Text>().text = "" + currentLvlPuissance;
            GameObject.FindGameObjectWithTag("po").GetComponent<Text>().text = "" + currentPo;
			GameObject.FindGameObjectWithTag("puissancePo").GetComponent<Text>().text = "" + listePuissance[currentLvlPuissance];
        }
	}
	
}