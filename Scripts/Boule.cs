using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boule
{
        public int vie { get; set; }
        public Vector3 pos { get; set; }
        public int timingSpawn { get; set; }
        public int taille { get; set; }
        public Boule lb_child1;
        public Boule lb_child2;
        private Vector3 border_lt, border_rb;

        public Boule(int vie, Vector3 pos, int timingSpawn, int taille)  
        {  
            this.vie = vie;
            this.pos = pos;  
            this.timingSpawn = timingSpawn;
            this.taille = taille;

            if (taille >= 2){
                if( vie % 2 == 0 ){
                    lb_child1 = new Boule(vie/2, pos, 0,taille - 1);
                    lb_child2 = new Boule(vie/2, pos, 0,taille - 1);
                } else {
                    int v = (int) System.Math.Floor(vie/2f);
                    lb_child1 = new Boule(v+1, pos, 0,taille - 1);
                    lb_child2 = new Boule(v, pos, 0, taille - 1);
                }

            }
        }

        public override string ToString()
        {
            return "Boule "+ this.vie+" hp // taille = " + this.taille;
        }

        public Boule(){}
}
