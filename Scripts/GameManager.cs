using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GameManager : MonoBehaviour
{
    private Vector3 border_rb, border_lt;
    private double lastInterval;

    private List<Level> levels;
    private Level currentLevel;
    private int currentNumLvl, currentLvlCadence, currentLvlPuissance,currentLvlUlti, currentPo;
    private List<Boule> listeBoules;
    private int current_num_boule;

    private float timer;
    private GameObject canon, win, lose, tap;


   
    void Start()
    {
        currentPo = PlayerPrefs.GetInt("po", 0);
        border_lt = Camera.main.ViewportToWorldPoint (new Vector3(0,1,0));
        lastInterval = Time.realtimeSinceStartup;
        currentNumLvl = PlayerPrefs.GetInt("numLvl", 1);
        GameObject.FindGameObjectWithTag("lvl").GetComponent<Text>().text = "" + currentNumLvl;
        levels = new List<Level>();
        levels.Add(new Level(1, 1, 2, 11)); //(num level, min d'une boule, max d'une boule, total somme des boules)
        levels.Add(new Level(2, 1, 4, 16));
        levels.Add(new Level(3, 3, 12, 25));
        currentLevel = levels[currentNumLvl - 1];
    
        listeBoules = currentLevel.createLvl();
        current_num_boule = 0;
        timer = 0;
        canon = GameObject.FindGameObjectWithTag("Player");

        //pour la fin de la partie on affiche perdu ou gagner en remettant actif le gameObject Text
        win = GameObject.FindGameObjectWithTag("win");
        tap = GameObject.FindGameObjectWithTag("tap");
        lose = GameObject.FindGameObjectWithTag("lose");
        win.gameObject.SetActive(false);
        lose.gameObject.SetActive(false);
        tap.gameObject.SetActive(false);
    }


    void Update()
    {
        GameObject.FindGameObjectWithTag("po").GetComponent<Text>().text = "" + currentPo;
        if (!canon.GetComponent<moveCanon>().getMove()){
            //lancer script de lose
            lose.gameObject.SetActive(true);
            PlayerPrefs.SetInt("po", currentPo);
            tap.gameObject.SetActive(true);
        }

        timer += Time.deltaTime;
        if (current_num_boule < listeBoules.Count -1){ //on instantiate les boules une par une en attendant le timingSpawn des boules
            if (timer > listeBoules[current_num_boule].timingSpawn)
            {
                current_num_boule += 1;
                GameObject b = Instantiate(Resources.Load("life_boul_" + listeBoules[current_num_boule].taille), listeBoules[current_num_boule].pos, Quaternion.identity) as GameObject;
                b.GetComponent<moveBoule>().setVie(listeBoules[current_num_boule].vie);
                if (listeBoules[current_num_boule].taille > 1) b.GetComponent<moveBoule>().setChilds(listeBoules[current_num_boule].lb_child1, listeBoules[current_num_boule].lb_child2);
                timer = 0;
            }
        }

        if (current_num_boule == listeBoules.Count -1 && GameObject.FindGameObjectsWithTag("boul").Length == 0){
            //lancer script de win
            win.gameObject.SetActive(true);
            tap.gameObject.SetActive(true);
            winLvl();
            PlayerPrefs.SetInt("po", currentPo);
        }
            
    }

    void winLvl(){
        PlayerPrefs.SetInt("numLvl", currentNumLvl+1);
    }

    public void addPo(){
        currentPo++;
    }
}
