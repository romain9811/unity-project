using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveBoule : MonoBehaviour
{
    private Vector3 borderGround_lb, border_rt;
    private Vector2 siz;
    private Vector2 movement;

    public int taille;
    private int vie;
    private float horizontal_speed, rotation_speed;
    private bool flotter;

    private Boule lb_child1;
    private Boule lb_child2;


   
    void Start()
    {
        flotter = false;
        horizontal_speed = Random.Range(-1.5f, 1.5f);
        if ((horizontal_speed * horizontal_speed) / horizontal_speed < 0.4) horizontal_speed = Random.Range(0.5f, 1.5f);
        if (horizontal_speed > 0) rotation_speed = -0.12f;
        
        GameObject ground = GameObject.FindGameObjectWithTag("ground");
        borderGround_lb = ground.GetComponent<SpriteRenderer>().transform.TransformPoint(new Vector3(ground.GetComponent<SpriteRenderer>().sprite.bounds.min.x, ground.GetComponent<SpriteRenderer>().sprite.bounds.max.y, 0));
        border_rt = Camera.main.ViewportToWorldPoint (new Vector3(1,0,0));
        siz.x = GetComponent<SpriteRenderer>().bounds.size.x;
        siz.y = GetComponent<SpriteRenderer>().bounds.size.y;

        //movement
        float[] listeHauteurSaut = {5.9f,6.5f,7.2f,7.8f}; //hauteur de saut en fonction de la taille
        movement = new Vector2(horizontal_speed, listeHauteurSaut[taille - 1]);
        GetComponent<Rigidbody2D>().velocity = new Vector2(horizontal_speed, 0);
    }

    void Update()
    {   
        //1.5 / 2.4 / 3.2 / 4.7
        //GetComponent<TextMeshProUGUI>().text = vie.ToString();
        GetComponent<Rigidbody2D>().rotation += rotation_speed;
        //faire rebondir la boule sur les cotés de l'ecran 
        if (transform.position.x > border_rt.x - siz.x / 1.8 || transform.position.x < borderGround_lb.x + siz.x / 1.8) {
            movement.x *= -1;
            rotation_speed *= -1;
            GetComponent<Rigidbody2D>().velocity = new Vector2(movement.x, GetComponent<Rigidbody2D>().velocity.y);
        }

        //faire rebondir la boule au sol
        if (transform.position.y < borderGround_lb.y + siz.y / 2) {
            GetComponent<Rigidbody2D>().velocity = movement;
            GetComponent<Rigidbody2D>().velocity = movement;
            flotter = true;
        }

        //faire + flotter la boule
        if (flotter && GetComponent<Rigidbody2D>().velocity.y < 0){
            GetComponent<Rigidbody2D>().velocity =  new Vector2(movement.x, 0.1f);
            flotter = false;
        }

    }

    public void setVie(int v){
        vie = v;
    }

    public int getVie(){
        return vie;
    }

    public Boule getBoule1(){
        return lb_child1;
    }

    public Boule getBoule2(){
        return lb_child2;
    }

    public void setChilds(Boule b1, Boule b2){
        lb_child1 = b1;
        lb_child2 = b2;
    }

    public void setVelocityChild(GameObject b1, GameObject b2){
        if (b1.GetComponent<Rigidbody2D>().velocity.x > 0  && b1.GetComponent<Rigidbody2D>().velocity.x > 0) movement.x *= -1;
        b1.GetComponent<Rigidbody2D>().velocity =  new Vector2(movement.x, 4f + taille);
        b2.GetComponent<Rigidbody2D>().velocity =  new Vector2(movement.x, 4f + taille);
    }

    void createBoule(){
        //
    }

}
