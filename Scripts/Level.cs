using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level
    {
        public int num { get; set; }
        public int min { get; set; }
        public int max { get; set; }
        public int total { get; set; }
        private Vector3 border_lt, border_rb;

        public Level(int num, int min, int max, int total)  
        {  
            this.num = num;
            this.min = min;  
            this.max = max + 1;  
            this.total = total;  
        }

        public override string ToString()
        {
            return "Level "+ this.num+" : total = " + this.total;
        }

        public List<Boule> createLvl(){
            // cette fonction va creer toutes les bases d'un niveau
            // 1 -> separer en un nombre aleatoire de boule le total entre min et max pour chaque boule // somme de la vie de chaque boule = total
            // 2 -> donner un timing d'apparition/position/taille pour chaque boule
            border_lt = Camera.main.ViewportToWorldPoint (new Vector3(0,1,0));
            border_rb = Camera.main.ViewportToWorldPoint (new Vector3(1,0,0));
            int total_tmp = total;
            List<Boule> listeBoules = new List<Boule>();
            int life = 0;
            int taille = 0;
            Boule new_boule;
            Vector3	tmpPos;
            while (total_tmp != 0) {
                if (total_tmp - this.max < min){
                    for (int i = this.min; i <= this.max; i++){
                        if (total_tmp - i >= min){
                            life = Random.Range(this.min, i);
                            break;
                        } 
                    }
                } else if(total_tmp - this.max == min) {
                    life =  this.min;
                } else {
                    life = Random.Range(this.min, this.max);
                }

                tmpPos = new Vector3(Random.Range(border_lt.x, border_rb.x),
                    border_lt.y * 1.2f,
                    0);
                taille = Random.Range(1,4);
                new_boule = new Boule(life, tmpPos, 4, taille);
                listeBoules.Add(new_boule);
                total_tmp -= life;
            }
            return listeBoules;
        }
    }
