using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;	

public class wait2sec : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        Invoke("goScene2",2.0f);
    }

    void goScene2(){							
        SceneManager.LoadScene("scene2-Menu");		
    }
}
