using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveCanon : MonoBehaviour
{

    private Vector2 siz;
    private float cadence, reset;
    private bool move;
    

    void Start()
    {
        siz.x =	gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
        siz.y = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;
        cadence =  0.8f - (PlayerPrefs.GetInt("cadenceLvl", 1) * 0.1f);
        reset = cadence;
        move = true;
    }

    void Update()
    {
        if (move) {
            reset -= Time.deltaTime;
            if(Input.GetMouseButton(0)){
                Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                pos.z=0;
                pos.y = transform.position.y;
                transform.position = pos;
                if ( reset < 0 )
                {
                    Shoot();
                    reset = cadence;
                }
            }
        } else {
            
        }
    }

    void Shoot(){
        Vector3	tmpPos = new Vector3(transform.position.x,
            transform.position.y + siz.y / 2,
            transform.position.z);
        GameObject gY =	Instantiate(Resources.Load("munition"), tmpPos, Quaternion.identity) as GameObject;
    }

    void OnTriggerEnter2D(Collider2D col)
        {
            if(col.name.Contains("life_boul")) {
                move = false;
            }
        }

    public bool getMove(){
        return move;
    } 

}
