using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveShoot : MonoBehaviour
{ 
    private Vector3 border_lt;
    private Vector2 siz;
    private int puissance;

    void Start(){
        GetComponent<Rigidbody2D>().velocity = new Vector2(0, 11); //vitesse des munitions
        border_lt = Camera.main.ViewportToWorldPoint (new Vector3(0,1,0));
        siz.y = GetComponent<SpriteRenderer>().bounds.size.y;
        puissance = PlayerPrefs.GetInt("puissanceLvl", 1); //puissance munition
    }
    

    void Update()
    {
        if(transform.position.y > border_lt.y + siz.y / 2){
            Destroy(gameObject);
        }
    }

     void OnTriggerEnter2D(Collider2D col)
        {
            if(col.name.Contains("boul")) {
                Destroy(gameObject);
                col.gameObject.GetComponent<moveBoule>().setVie(col.gameObject.GetComponent<moveBoule>().getVie() - puissance );
                
                int new_vie = col.gameObject.GetComponent<moveBoule>().getVie();
                if (new_vie <= 0){
                    Destroy(col.gameObject);
                    if (col.gameObject.GetComponent<moveBoule>().taille > 1) { //chaque boule dont la taille est > 1 à 2 enfant qui spawn dès que le parent est destroy
                        Boule b1 = col.gameObject.GetComponent<moveBoule>().getBoule1(); //on recup les enfants
                        Boule b2 = col.gameObject.GetComponent<moveBoule>().getBoule2();
                        GameObject b_inst1 = Instantiate(Resources.Load("life_boul_" + b1.taille), b1.pos, Quaternion.identity) as GameObject; //on instantie les enfants
                        GameObject b_inst2 = Instantiate(Resources.Load("life_boul_" + b2.taille), b2.pos, Quaternion.identity) as GameObject;
                        b_inst1.GetComponent<moveBoule>().setVie(b1.vie);
                        b_inst2.GetComponent<moveBoule>().setVie(b2.vie);
                        b_inst1.GetComponent<SpriteRenderer>().transform.position = col.gameObject.GetComponent<SpriteRenderer>().transform.position;
                        b_inst2.GetComponent<SpriteRenderer>().transform.position = col.gameObject.GetComponent<SpriteRenderer>().transform.position;
                        b_inst1.GetComponent<moveBoule>().setChilds(b1.lb_child1, b1.lb_child1); //on ajoute aux instance des enfants crée leurs 2 enfants
                        b_inst2.GetComponent<moveBoule>().setChilds(b2.lb_child2, b2.lb_child2);
                    }
                    GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().addPo();
                }
            }
        }   
}
