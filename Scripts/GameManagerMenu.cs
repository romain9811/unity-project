using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManagerMenu : MonoBehaviour
{
    private int currentNumLvl,currentLvlCadence,currentLvlPuissance,currentPo;
    private List<int> listePuissance;
    private List<int> listeCadence;
       

    void Start()
    {
        currentNumLvl = PlayerPrefs.GetInt("numLvl", 1);
        Debug.Log(currentNumLvl);
        currentLvlCadence = PlayerPrefs.GetInt("cadenceLvl", 1);
        currentLvlPuissance = PlayerPrefs.GetInt("puissanceLvl", 1);
        currentPo = PlayerPrefs.GetInt("po", 0);
        listePuissance = new List<int>{7,22,37,45,73};        
        listeCadence =  new List<int>{8,13,19,32,42};
        GameObject.FindGameObjectWithTag("po").GetComponent<Text>().text = "" + currentPo;
        GameObject.FindGameObjectWithTag("cadencePo").GetComponent<Text>().text = "" + listeCadence[currentLvlCadence];
        GameObject.FindGameObjectWithTag("puissancePo").GetComponent<Text>().text = "" + listePuissance[currentLvlPuissance];
        GameObject.FindGameObjectWithTag("cadenceLvl").GetComponent<Text>().text = "" + currentLvlCadence;
        GameObject.FindGameObjectWithTag("puissanceLvl").GetComponent<Text>().text = "" + currentLvlPuissance;
        GameObject.FindGameObjectWithTag("lvl").GetComponent<Text>().text = "" + currentNumLvl;
    }

    void Update(){

    }
}
